
package web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import beans.User;
import service.IDaoRemote;



@WebServlet("/UserController")
public class ControlleurServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
    private static String redirect="usersJsp.jsp";

	@EJB
	private IDaoRemote service;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControlleurServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<User> users = service.findAll();
		request.setAttribute("users", users);
		request.getRequestDispatcher(redirect).forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		// TODO Auto-generated method stub
		if (action.equalsIgnoreCase("add")) {
			String Nom = request.getParameter("nom");
			String Prenom = request.getParameter("prenom");
			String Tele = request.getParameter("tele");
			String email = request.getParameter("email");
			String dateNaiss = request.getParameter("dateNaiss").replace("-", "/");
			service.create(new User(Nom,Prenom,Integer.parseInt(Tele),email, new Date(dateNaiss)));
			
		}
		else if(action.equalsIgnoreCase("delete")) {
			System.out.println("hello");

			//String userid = request.getParameter("id");
			long id=Long.parseLong(request.getParameter("id"));
			service.delete(id);
			
			List<User> users = service.findAll();
			request.setAttribute("users", users);
		}
		else if(action.equalsIgnoreCase("editUser")) {
			System.out.println("hello");

			String userid = request.getParameter("id");
			int id=Integer.parseInt(userid);
			User user=service.findById(id);
			request.setAttribute("user", user);
        	//redirect = "/editUser.jsp";            

			/*List<User> users = service.findAll();
			request.setAttribute("users", users);*/
		}else if(action.equalsIgnoreCase("update")) {
			String userid = request.getParameter("id");
			int id=Integer.parseInt(userid);
			String Nom = request.getParameter("nom");
			String Prenom = request.getParameter("prenom");
			String Tele = request.getParameter("tele");
			String email = request.getParameter("email");
			String dateNaiss = request.getParameter("dateNaiss").replace("-", "/");
			service.update(new User(id,Nom,Prenom,Integer.parseInt(Tele),email, new Date(dateNaiss)));
		}
		//response.setContentType("application/json");
		
		doGet(request, response);
		
	}
	
}
