package service;

import java.util.List;

import javax.ejb.Remote;

import beans.Smartphone;

@Remote
public interface IDaoRemoteSmartphone {
	void create(Smartphone s) ;
	boolean update(Smartphone s) ;
	boolean delete(int id) ;
	Smartphone findById(int id) ;
	List<Smartphone> findAll() ;
}
