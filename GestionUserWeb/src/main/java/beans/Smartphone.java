package beans;

import beans.User;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Smarthone
 *
 */
@Entity

public class Smartphone implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String imei;
	private static final long serialVersionUID = 1L;
	@Transient
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user")
    private User user;
	
	
	public Smartphone(int id, String imei, User user) {
		super();
		this.id = id;
		this.imei = imei;
		this.user = user;
	}
	
	public Smartphone(int id, String imei) {
		super();
		this.id = id;
		this.imei = imei;
	}

	public Smartphone(String imei, User user) {
		super();
		this.imei = imei;
		this.user = user;
	}
	public Smartphone() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getImei() {
		return this.imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}   
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Smarthone [id=" + id + ", imei=" + imei + ", user=" + user + "]";
	}
   
}
