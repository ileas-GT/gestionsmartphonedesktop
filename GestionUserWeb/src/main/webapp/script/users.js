$(document).ready(function() {

    $.ajax({
        url: "UserController",
        data: { op:"load" },
        method: "POST",
        success: function(data) {
            remplir(data);
        }
    });

	$("#add").click(function() {
	    var nom = $("#nom").val();
		var prenom = $("#prenom").val();
		var tele = $("#tele").val();
		var email = $("#email").val();
		var dateNaiss = $("#dateNaiss").val();
		
		$.ajax({
			url: "UserController",
			data: {nom:nom, prenom:prenom, tele:tele, email:email, dateNaiss:dateNaiss},
			method: "POST",
			success: function(data) {
				remplir(data);
			}
		});
	});

	function remplir(data) {
		var ligne = "";
		data.forEach(e => {
			ligne += "<tr><td>" + e.Nom + "</td><td>" + e.Prenom + "</td><td>" + e.Tele + "</td><td>" + e.email + "</td><td>" + e.dateNaiss + "</td><td>Supprimer</td><td>Modifier</td></tr>";
		});
		$("#content").html(ligne);

	}

});