<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="script/jquery-3.5.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>



<body>
	<fieldset>
		<legend> Informations User </legend>
		<form action="UserController" method="post">
		<input type="hidden" name="action" value="add" />
		 <div class="form-group">
    <label for="exampleInputEmail1">Nom</label>
    <input type="text" class="form-control" name="nom" placeholder="Enter name">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">prenom</label>
    <input type="text" class="form-control" name="prenom"  placeholder="Enter prenom">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Tele</label>
    <input type="number" class="form-control" name="tele"  placeholder="Enter tele">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Date Naissance</label>
    <input type="Date" class="form-control" name="dateNaiss"  placeholder="Enter Date Naissance">
  </div>
 
  <input type="submit" value="add" class="btn btn-primary"/>
	</fieldset>
</form>
	<fieldset>
		<legend> Liste des Users </legend>
		<table border="1" class="table table-dark">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Prenom</th>
					<th>tele</th>
					<th>email</th>
					<th>Date naissance</th>
					<th>Supprimer</th>
					<th>Modifier</th>
				</tr>
				<c:forEach items="${users}" var="c">
				<tr>
					<th>${c.getNom() }</th>
					<th>${c.getPrenom() }</th>
					<th>${c.getTele() }</th>
					<th>${c.getEmail() }</th>
					<th>${c.getDateNaiss() }</th>
					<th> <a href="UserController?action=delete$id=${c.getId()}" class="btn btn-danger">delete</a></th>
					<th>
					<a href="editUser.jsp?action=editUser&userId=${c.getId()}" class="btn bnt-primary">Edit</a>
				
					</th>
				</tr>
				</c:forEach>
			</thead>
			<tbody id="content">
			</tbody>
		</table>
		
		
</body>
</html>