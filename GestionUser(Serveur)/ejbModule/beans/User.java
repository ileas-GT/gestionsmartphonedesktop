package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */

@Entity
public class User implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	private String Nom;
	private String Prenom;
	private int Tele;
	private String email;
	private Date dateNaiss;
	@Transient
	@OneToMany(fetch = FetchType.EAGER, mappedBy ="user")
    private List<Smartphone> smartphone;

	

	public User(String nom) {
		super();
		Nom = nom;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Tele=" + Tele + ", email=" +email+ " ,dateNaiss="
				+ dateNaiss + "]";
	}


	public User() {
		super();
	}


	public User(long id, String Nom, String Prenom, int Tele,String email, Date dateNaiss) {
		super();
		this.id = id;
		this.Nom = Nom;
		this.Prenom = Prenom;
		this.Tele = Tele;
		this.email=email;
		this.dateNaiss = dateNaiss;
	}


	public User(String Nom, String Prenom, int Tele,String email, Date dateNaiss) {
		super();
		
		this.Nom = Nom;
		this.Prenom = Prenom;
		this.Tele = Tele;
		this.email=email;
		this.dateNaiss = dateNaiss;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNom() {
		return Nom;
	}


	public void setNom(String nom) {
		Nom = nom;
	}


	public String getPrenom() {
		return Prenom;
	}


	public void setPrenom(String prenom) {
		Prenom = prenom;
	}


	public int getTele() {
		return Tele;
	}


	public void setTele(int tele) {
		Tele = tele;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Date getDateNaiss() {
		return dateNaiss;
	}


	public void setDateNaiss(Date dateNaiss) {
		this.dateNaiss = dateNaiss;
	}
	

   
}
