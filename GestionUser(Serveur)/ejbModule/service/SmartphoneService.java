package service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import beans.Smartphone;
import beans.User;

@Stateless
public class SmartphoneService implements IDaoRemoteSmartphone{
	@PersistenceContext(unitName="GestionUser(Serveur)")
	EntityManager em;
	public SmartphoneService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void create(Smartphone m)  {
		try {
			
			
			em.persist(m);
			System.out.println("created sucesss");
			
		      } catch (Exception e) {
			System.out.println(e);
		      }
	}

	@Override
	public boolean update(Smartphone m)  {
		// TODO Auto-generated method stub
		try {
			
			
			
			Smartphone smartphone=em.find(Smartphone.class,m.getId());
			smartphone.setImei(m.getImei());
			smartphone.setUser(m.getUser());
			
			em.persist(smartphone);
			System.out.println("update sucesss");
			
		      } catch (Exception e) {
			System.out.println(e);
		      }
		return false;
	}

	@Override
	public boolean delete(int id) {
		try {
			
			
			Smartphone m=em.find(Smartphone.class,id);
			em.remove(m);
			System.out.println("delete sucesss");
			
		} catch (Exception e) {
			System.out.println(e);
		      }
		return false;
	}

	@Override
	public Smartphone findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Smartphone> findAll()  {
		// TODO Auto-generated method stub
		List<Smartphone> ListSmart=new ArrayList<Smartphone>();
		
		
		javax.persistence.Query query =em.createQuery("from Smartphone");
		ListSmart=query.getResultList();
		
		
		
		return ListSmart;
	}
}
