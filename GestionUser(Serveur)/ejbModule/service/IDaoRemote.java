package service;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Remote;

import beans.User;

@Remote
public interface IDaoRemote {
	    void create(User o) ;
		boolean update(User o) ;
		boolean delete(long id) ;
		User findById(int id) ;
		User findByName(String name);
		List<User> findAll() ;
}
