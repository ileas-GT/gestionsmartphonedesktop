package service;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Local;

import beans.User;

@Local
public interface IDao<T>  {
	 void create(T o) ;
		boolean update(T o);
		boolean delete(long id);
		T findById(int id);
		List<T> findAll() ;
}
