package service;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import beans.User;

@WebService
@SOAPBinding
public class UtilisateurServiceSoap {
	@EJB
	IDaoRemote idao;
    
	public void create(User user) {
		idao.create(user);
	}
	
	public List<User> findAll() {
		return idao.findAll();
	}
	
	public boolean delete(long id) {
		return idao.delete(id);
	}
	
	public boolean update(User user) {
		return idao.update(user);
	}
	
	public User findById(int id) {
		return idao.findById(id);
	}
	public User findByName(String nom) {
		return idao.findByName(nom);
	}
	
}
