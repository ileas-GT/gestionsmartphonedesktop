package service;


import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import beans.User;

@Stateless
public class UtilisateurService implements IDao<User>,IDaoRemote{

	@PersistenceContext(unitName="GestionUser(Serveur)")
	EntityManager em;
	public UtilisateurService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	

	

	@Override
	public void create(User m)  {
		try {
			
			
			em.persist(m);
			System.out.println("created sucesss");
			
		      } catch (Exception e) {
			System.out.println(e);
		      }
	}

	@Override
	public boolean update(User m)  {
		// TODO Auto-generated method stub
		try {
			
			
			
			User user=em.find(User.class,m.getId());
			user.setNom(m.getNom());
			user.setPrenom(m.getPrenom());
			user.setTele(m.getTele());
			user.setEmail(m.getEmail());
			user.setDateNaiss(m.getDateNaiss());
			em.persist(user);
			System.out.println("update sucesss");
			
		      } catch (Exception e) {
			System.out.println(e);
		      }
		return false;
	}

	@Override
	public boolean delete(long id) {
		try {
			
			
			User m=em.find(User.class,id);
			em.remove(m);
			System.out.println("delete sucesss");
			
		} catch (Exception e) {
			System.out.println(e);
		      }
		return false;
	}

	@Override
	public User findById(int id) {
		// TODO Auto-generated method stub
		User m=em.find(User.class,id);
        return m;
		
	}
	public User findByName(String name) {
		// TODO Auto-generated method stub
		
			User m= em.createQuery("select u from User u where u.Nom = :name",User.class)
					.setParameter("name", name)
					.getSingleResult();
	                

			return m;

		
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAll()  {
		// TODO Auto-generated method stub
		List<User> ListUser=new ArrayList<User>();
		
		
		javax.persistence.Query query =em.createQuery("from User");
		ListUser=query.getResultList();
		
		
		
		return ListUser;
	}

}
